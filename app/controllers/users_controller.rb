class UsersController < ApplicationController
  before_action :generate_users, only: [:index]
  def index
    render json: @users
  end

  private
  def generate_users
    number = params[:number].nil? ? nil : params[:number].to_i
    @users = UserGenerator.generate(number)
  end
end

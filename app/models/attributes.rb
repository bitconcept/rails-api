module Attributes
  USER_ATTR = {
    "Internet" => [:email],
    "Name" => [:name],
    "Address" => [:city, :street_address]
  }
end

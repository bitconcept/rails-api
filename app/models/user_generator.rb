class UserGenerator
  include Faker

  @methods = {
    "Internet" => [:email],
    "Name" => [:name],
    "Address" => [:street_address, :city],
    "PhoneNumber" => [:cell_phone],
    "Company" => [:profession]
  }

  def self.generate(number=nil)
    users = []
    (number.nil? ? 10 : number).times do |n|
      id = (n+1)
      tmp_store = {id=>{}}
      @methods.each do |constant, method|
        @methods[constant].each { |key| tmp_store[id].store( key.to_sym, Faker.const_get(constant).public_send(key) )}
      end
      users << tmp_store
    end
    users.to_json
  end
end

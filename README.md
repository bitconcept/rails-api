----
## Ruby API for React

> This API consumes others API and provides an easy way to get consumed. If you want to collaborate be free to do it.  
Note: This API uses paginate for all resources. 

----
## Usage

* Get all users [:email, :fullname, :city, :phone, :work, :address]  
By default if you don't provide any route, users resource will taken like root page.

**GET**: http://localhost:3000  

If you want only n users you could use the number param.

**Using pagination**: https://localhost:3000/?number=2

----
## Thanks
Email: devpolish@protonmail.com  
Twitter: devpolish0x0a  
Demo: https://api-only.herokuapp.com
